package fr.epita.tp2.exposition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.epita.tp2.service.HelloService;

@RestController
@RequestMapping("/hello")
public class HelloController {
	
	@Autowired
	HelloService service;
	
	@GetMapping("/{name}/{firstname}")
	public String sayHello(@PathVariable("name") String nom,@PathVariable("firstname") String prenom) {
		
		return service.getHello(nom, prenom);
	}

}
