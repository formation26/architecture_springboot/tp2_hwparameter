package fr.epita.tp2.service;

public interface HelloService {
	
	String getHello(String nom, String prenom);

}
