package fr.epita.tp2.service;

import org.springframework.stereotype.Service;

@Service
public class HelloServiceImpl implements HelloService {

	@Override
	public String getHello(String nom, String prenom) {
		
		return "Hello "+nom + " "+prenom;
	}

}
